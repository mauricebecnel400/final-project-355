/**
 * Created by Mbecnel on 4/18/2017.
 */
var express = require('express');
var router = express.Router();
var customer_dal = require('../model/customer_dal');

// View All accounts
router.get('/all', function(req, res) {
    customer_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('customer/customerViewAll', { 'result':result });
        }
    });

});

// View the Job for the given id
router.get('/', function(req, res){
    if(req.query.contact_id == null) {
        res.send('contact_id is null');
    }
    else {
        customer_dal.getById(req.query.contact_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('customer/customerViewById', {'result': result});
            }
        });
    }
});

// Return the add a new Job form
router.get('/add', function(req, res){
    res.render('Job/accountAdd');
});

// View the Job for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(!(req.query.first_name && req.query.last_name && req.query.email)) {
        res.send('All fields must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        customer_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Job/all');
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.contact_id == null) {
        res.send('A contact id is required');
    }
    else {
        customer_dal.getById(req.query.contact_id, function (err, result) {
            res.render('customer/customerUpdate', {customer: result[0]});
        });
    }
});

router.get('/edit2', function(req, res){
    if(req.query.account_id == null) {
        res.send('A Job id is required');
    }
    else {
        customer_dal.getById(req.query.account_id, function(err, account){
            customer_dal.getAll(function(err, address) {
                res.render('Job/accountUpdate', {account: account[0], address: address});
            });
        });
    }

});

router.get('/update', function(req, res) {
    customer_dal.update(req.query, function(err, result){
        res.redirect(302, '/customer/all');
    });
});

// Delete a Job for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        customer_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/Job/all');
            }
        });
    }
});

module.exports = router;