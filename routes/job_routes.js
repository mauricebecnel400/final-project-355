/**
 * Created by Mbecnel on 4/18/2017.
 */
var express = require('express');
var router = express.Router();
var job_dal = require('../model/job_dal');

// View All addresss
router.get('/all', function(req, res) {
    job_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('job/jobViewAll', { 'result':result});
        }
    });
});

// View the jobs for the given id
router.get('/', function(req, res){
    if(req.query.job_id == null) {
        res.send('job_id is null');
    }
    else {
        job_dal.getById(req.query.job_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('job/jobViewById', {'result': result});
            }
        });
    }
});

// Return the add a new tech Report form
router.get('/add', function(req, res){
    job_dal.getCustomers(function(err, customers){
        if (err) {
            res.send(err);
        }
        else {
            console.log(customers);
            job_dal.getComputers(function(err, computers){
                if (err) {
                    res.send(err);
                }
                else {
                    console.log(computers);
                    res.render('job/jobAdd', {'contact': customers, 'computer': computers})
                }
            });
        }
    });
});

// View the tech Report for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(!(req.query.contact_id && req.query.job_description && req.query.job_status
        && req.query.location && req.query.computer_id)) {
        res.send('All * fields must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        job_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/job/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.job_id == null) {
        res.send('A job_id is required');
    }
    else {
        job_dal.getById(req.query.job_id, function(err, result){
            res.render('job/jobUpdate', {job: result[0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.address_id == null) {
        res.send('A tech Report id is required');
    }
    else {
        address_dal.getById(req.query.address_id, function(err, address){
            address_dal.getAll(function(err, address) {
                res.render('tech Report/addressUpdate', {address: address[0], address: address});
            });
        });
    }

});

router.get('/update', function(req, res) {
    job_dal.update(req.query, function(err, result){
        res.redirect(302, '/job/all');
    });
});

// Delete a tech Report for the given address_id
router.get('/delete', function(req, res){
    if(req.query.job_id == null) {
        res.send('job_id is null');
    }
    else {
        job_dal.delete(req.query.job_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/job/all');
            }
        });
    }
});

module.exports = router;