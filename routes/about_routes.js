/**
 * Created by Mbecnel on 4/18/2017.
 */
var express = require('express');
var router = express.Router();



// Return the add a new about form
router.get('/about', function(req, res){
    res.render('about/about');
});

module.exports = router;