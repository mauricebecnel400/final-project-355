/**
 * Created by Mbecnel on 4/18/2017.
 */
var express = require('express');
var router = express.Router();
var tech_dal = require('../model/tech_dal');

// View All skills
router.get('/all', function(req, res) {
    tech_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('tech/techViewAll', { 'result':result });
        }
    });

});

// View the about for the given id
router.get('/', function(req, res){
    if(req.query.social_security == null) {
        res.send('social_security is null');
    }
    else {
        tech_dal.getById(req.query.social_security, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('tech/techViewById', {'result': result});
            }
        });
    }
});

// Return the add a new about form
router.get('/add', function(req, res){
    res.render('tech/techAdd');
});

// View the about for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(!(req.query.name && req.query.social_security)) {
        res.send('All fields must be provided');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        tech_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/tech/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.skill_id == null) {
        res.send('A about id is required');
    }
    else {
        skill_dal.edit(req.query.skill_id, function(err, result){
            res.render('about/skillUpdate', {skill: result[0]});
        });
    }

});

router.get('/edit2', function(req, res){
    if(req.query.skill_id == null) {
        res.send('A about id is required');
    }
    else {
        skill_dal.getById(req.query.skill_id, function(err, skill){
            address_dal.getAll(function(err, address) {
                res.render('about/skillUpdate', {skill: skill[0], address: address});
            });
        });
    }

});

router.get('/update', function(req, res) {
    skill_dal.update(req.query, function(err, result){
        res.redirect(302, '/about/all');
    });
});

// Delete a about for the given skill_id
router.get('/delete', function(req, res){
    if(req.query.social_security == null) {
        res.send('socal_security is null');
    }
    else {
        tech_dal.delete(req.query.social_security, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/tech/all');
            }
        });
    }
});

module.exports = router;