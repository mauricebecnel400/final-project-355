/**
 * Created by Mbecnel on 4/18/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view skill_view as
 select s.*, a.street, a.zip_code from about s
 join tech Report a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'Select j.job_id, c.contact_id, c.first_name, c.last_name, j.job_description, j.job_status, j.location ' +
    'From job j Left join contact c on j.contact_id = c.contact_id ' +
    'Left Join computer co on j.computer_id = co.computer_id ' + 'Where job_status = "Job Incomplete" ' +
    'Group by j.job_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(skill_id, callback) {
    var query = 'SELECT * FROM about a WHERE a.skill_id = ?';
    var queryData = [skill_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO about (skill_name, description) VALUES (?, ?)';

    var queryData = [params.skill_name, params.description];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.delete = function(skill_id, callback) {
    var query = 'DELETE FROM about WHERE skill_id = ?';
    var queryData = [skill_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE about SET skill_name = ?, description = ? WHERE skill_id = ?';
    var queryData = [params.skill_name, params.description, params.skill_id];
    console.log(queryData);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(params, callback) {
    var query = 'select * from about WHERE skill_id = ?';
    var queryData = [params];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};