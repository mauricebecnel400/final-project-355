var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM techs;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(address_id, callback) {
    var query = 'SELECT * FROM techs where social_security = ?;';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};



exports.insert = function(params, callback) {

    // FIRST INSERT THE COMPANY
    var query = 'INSERT INTO techs (name, social_security) VALUES (?)';

    var queryData = [params.name, params.social_security];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(social_security, callback) {
    var query = 'DELETE FROM techs where social_security = ?';
    var queryData = [social_security];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};


exports.update = function(params, callback) {
    var query = 'UPDATE tech Report SET street = ?,  zip_code= ? WHERE address_id = ?';
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

/*  Stored procedure used in this example
 DROP PROCEDURE IF EXISTS account_getinfo;

 DELIMITER //
 CREATE PROCEDURE account_getinfo (_account_id int)
 BEGIN

 SELECT * FROM Job WHERE account_id = _account_id;

 END //
 DELIMITER ;

 # Call the Stored Procedure
 CALL account_getinfo (4);

 */

exports.edit = function(params, callback) {
    var query = 'select * from tech Report WHERE address_id = ?';
    var queryData = [params];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};