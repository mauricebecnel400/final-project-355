/**
 * Created by Mbecnel on 4/18/2017.
 */
var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

/*
 create or replace view company_view as
 select s.*, a.street, a.zip_code from tech s
 join tech Report a on a.address_id = s.address_id;

 */

exports.getAll = function(callback) {
    var query = 'Select j.job_id, c.contact_id, c.first_name, c.last_name, j.job_description, j.job_status, j.location ' +
        'From job j Left join contact c on j.contact_id = c.contact_id ' +
            'Left Join computer co on j.computer_id = co.computer_id ' +
        'Group by j.job_id;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getCustomers = function(callback) {
    var query = 'select c.contact_id, c.first_name, c.last_name from contact c;';
    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.getComputers = function(callback) {
    var query = 'select c.computer_id, c.serial_number, c.computer_type, c.model_year from computer c;'
    connection.query(query, function(err, result){
        callback(err, result);
    });
};

exports.getById = function(account_id, callback) {
    var query = 'select j.*, c.*, co.computer_type, co.model_year ' +
        'From job j ' +
        'Left join contact c on j.contact_id = c.contact_id ' +
        'Left join computer co on j.computer_id = co.computer_id ' +
        'Where j.job_id = ? ' +
        'Group by j.job_id; ';
    var queryData = [account_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

exports.insert = function(params, callback) {
    var query = 'INSERT INTO job (contact_id, repair_description, job_description, job_status, location, repair_cost, balanced_owed, computer_id) ' +
    'VALUES (?, ?, ?, ?, ?, ?, ?, ?);';

    var queryData = [params.contact_id, params.repair_description, params.job_description,
        params.job_status, params.location, params.repair_cost, params.balanced_owed, params.computer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.delete = function(job_id, callback) {
    var query = 'DELETE FROM job WHERE job_id = ?';
    var queryData = [job_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};



exports.update = function(params, callback) {
    var query = 'UPDATE job Set repair_description = ?, job_description = ?,  job_status = ?, location = ?, repair_cost = ?' +
        ', balanced_owed = ? WHERE job_id = ?';
    var queryData = [params.repair_description, params.job_description, params.job_satus, params.location, params.repair_cost,
     params.balanced_owed, params.job_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(params, callback) {
    var query = 'select * from Job WHERE account_id = ?';
    var queryData = [params];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};